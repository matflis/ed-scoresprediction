1. Cel projektu
---------------

Celem naszego projektu było zbadanie na do jakiego stopnia jesteśmy
wstanie przewidzieć rezultaty meczów zespołów piłkarskich wybranej ligi,
oraz ile na tym możemy potencjalnie na tym zarobić.

2. Generowanie danych, dane surowe i dane poprawne technicznie
--------------------------------------------------------------

Wszelkie dane pochodzą z: <http://www.football-data.co.uk/data.php>. Ze
względu na to, że ilość plików z danymi nie była znacząca, dane w
plikach .csv zostały pobrane ręcznie. Dodatkowo napisaliśmy skrypt
służący do pobrania przyszłych danych i przetworzenia danych do formy
czytelnej dla programu R Studio.

W naszym przypadku dane surowe były równoważne z danymi poprawnymi
technicznie.

Formula na pobieranie danych
www.football-data.co.uk/mmz4281/SEZON/LIGA.csv - Premier League - "E0""
- SerieA - "D1" - Primiera Division - "SP1" - Bundesliga - "D1"

Przykladowy url dla sezonu 09/10 dla Premier League -
www.football-data.co.uk/mmz4281/0910/E0.csv

3. Czyszczenie danych
=====================

    league <- c('Bundesliga', 'Bundesliga 2', 'LaLiga', 'PremierLeague', 'Championship', 'SerieA', 'Ligue 1', 'Turcja')
    # Wybrana liga do analizy
    choice <- 3
    # Zakres danych doczytaczych wynikow
    data_range <- 14:15
    # Zakres danych walidacyjnych
    validation_data_range <- 16:16

    league_data <- read_scores(league[choice], data_range)
    validation_data <- read_scores(league[choice], validation_data_range)

    total_data <- c(league_data, validation_data)

4. Analiza danych
-----------------

Statystyki procentowe poszczególnych zespołów prezentują sie
następująco.

    team_names <- find_unique_teams(league_data)

    teams <- calculate_scores_statistics(team_names, league_data)

    ## Percentage comparison between Win-Draw-Loose (Home)


    barplot(t(teams[,5:7]),
            names.arg = teams$Team,
            cex.names=1,
            las=2,
            col=c("blue", "green", "red"),
            #xlab="Team",
            ylab="Win | Draw | Loose scores",
            xlim=c(0,30),width=1,
            ylim=c(0,1))

    title("Win-Draw-Loose (Home)")
    legend("topright",
           legend = colnames(teams[5:7]), pch=15, col=c("blue", "green", "red"))

![](presentation_files/figure-markdown_strict/statistics-1.png)

    ## Percentage comparison between Win-Draw-Loose (Away)

    barplot(t(teams[,11:13]),
            names.arg = teams$Team,
            cex.names=1,
            las=2,
            col=c("blue", "green", "red"),
            #xlab="Team",
            ylab="Win | Draw | Loose scores",
            xlim=c(0,30),width=1,
            ylim=c(0,1))

    title("Win-Draw-Loose (Away)")
    legend("topright",
           legend = colnames(teams[11:13]), pch=15, col=c("blue", "green", "red"))

![](presentation_files/figure-markdown_strict/statistics-2.png)

    ## Home vs. Away Scores - Win
    barplot(t(teams[,c(2,8)]),
            names.arg = teams$Team,
            cex.names=1,
            las=2,
            col=c("blue","red"),
            #xlab="Team",
            ylab="Home/Away scores",
            xlim=c(0,70),width=1,
            ylim=c(0,max(teams$HomeWin,teams$AwayWin)),
            beside= TRUE)

    title("Win - Home vs. Away")
    legend("topright", 
           legend = colnames(teams[c(2,8)]), pch=15, col=c("blue","red"))

![](presentation_files/figure-markdown_strict/statistics-3.png)

    ## Home vs. Away Scores - Draw
    barplot(t(teams[,c(3,9)]),
            names.arg = teams$Team,
            cex.names=1,
            las=2,
            col=c("blue","red"),
            #xlab="Team",
            ylab="Home/Away scores",
            xlim=c(0,70),width=1,
            ylim=c(0,max(teams$HomeDraw,teams$AwayDraw)),
            beside= TRUE)

    title("Draw - Home vs. Away")
    legend("topright", 
           legend = colnames(teams[c(3,9)]), pch=15, col=c("blue","red"))

![](presentation_files/figure-markdown_strict/statistics-4.png)

    ## Home vs. Away Scores - Loose
    barplot(t(teams[,c(4,10)]),
            names.arg = teams$Team,
            cex.names=1,
            las=2,
            col=c("blue","red"),
            #xlab="Team",
            ylab="Home/Away scores",
            xlim=c(0,70),width=1,
            ylim=c(0,max(teams$HomeLoose,teams$AwayLoose)),
            beside= TRUE)

    title("Loose - Home vs. Away")
    legend("topright", 
           legend = colnames(teams[c(4,10)]), pch=15, col=c("blue","red"))

![](presentation_files/figure-markdown_strict/statistics-5.png)

    ## Ogólne porównanie wyników drużyn - Home
     
     barplot(t(teams[,2:4]),
             names.arg = teams$Team,
             cex.names=1,
             las=2,
             col = c("blue", "green", "red"),
             #xlab="Team",
             ylab="Win | Draw | Loose scores",
             xlim=c(0,90),width=1,
             ylim=c(0,max(teams$HomeWin,teams$HomeDraw,teams$HomeLoose)),
             beside= TRUE)

     title("Win-Draw-Loose (Home)")
    legend("topright", 
             legend = colnames(teams[2:4]), pch=15, col = c("blue", "green", "red"))

![](presentation_files/figure-markdown_strict/statistics-6.png)

     ## Ogólne porównanie wyników drużyn - Away

     barplot(t(teams[,8:10]),
             names.arg = teams$Team,
             cex.names=1, 
             las=2,
             col = c("blue", "green", "red"),
             #xlab="Team",
             ylab="Win | Draw | Loose scores",
             xlim=c(0,90),width=1,
             ylim=c(0,max(teams$AwayWin,teams$AwayDraw,teams$AwayLoose)),
             beside= TRUE)

     title("Win-Draw-Loose (Away)")
    legend("topright", 
             legend = colnames(teams[8:10]), pch=15, col = c("blue", "green", "red"))

![](presentation_files/figure-markdown_strict/statistics-7.png)

5. Testy
========

Statystyka Win-Draw-Loose
=========================

Zliczanie statystyk i wnioskowanie na podstawie funkcji
prawdopodobieństwa Wzory to: Sukces - 3/5 \* HomeWin + 1/2 \* AwayLoose
Remis - 1/2 \* HomeDraw + 1/2 \* AwayDraw Porażka - Reszta

Dodatkowo została uwzględniona randomizacja - tj. dla niewielkiej
różnicy statystyk % możliwy jest inny wynik niż ten zgodny z zasadą
najwyższego prawdopodobieństwa.

    home_win_weight <- 3/5
    home_draw_weight <-  1/2

    away_loose_weight <- 1/2
    away_draw_weight <- 1/2

    h_d_a <- calculate_naive_result(teams, validation_data)
    h_d_a

    ##   Dokladnosc Zysk
    ## 1   49.21053  112

    h_a_fixed <- data.frame()
    h_a_randomized <- data.frame()

    for(i in 1:length(total_data)) {
      if(i < length(total_data)) {
        training_data <- total_data[(i+1):length(total_data)]
      }
      else {
        training_data <- c()
      }
      if(i > 1) {
        training_data <- c(total_data[1:i - 1], training_data)
      }
      val_data <- total_data[i]
      team_stats <- calculate_scores_statistics(team_names, training_data)
      h_a_fixed <- rbind(h_a_fixed, calculate_naive_ha_result(team_stats, val_data, home_win_weight, home_draw_weight, away_draw_weight, away_loose_weight, FALSE))
      h_a_randomized <- rbind(h_a_randomized, calculate_naive_ha_result(team_stats, val_data, home_win_weight, home_draw_weight, away_draw_weight, away_loose_weight, TRUE))
    }

    h_a_fixed

    ##   Dokladnosc  Zysk
    ## 1   56.05263  44.1
    ## 2   55.52632 130.7
    ## 3   56.05263  44.1
    ## 4   55.52632 130.7

    h_a_randomized

    ##   Dokladnosc   Zysk
    ## 1   54.47368 -144.9
    ## 2   54.47368  -38.4
    ## 3   55.00000  -20.2
    ## 4   53.94737  -38.1

Metoda z wykorzystaniem rang - ELO
==================================

Dokładny algorytm opisany jest na -
<http://footballdatabase.com/methodology.php>

Generalny wzór na nowy ranking to: - R\_n = R\_o + K*G * (W - W\_e)
Gdzie; R\_n = Nowy rating R\_o = Stary rating K = Waga meczu (w
zaleznosci od poziomu rozgrywek) G = Index różnicy goli W = Rezultat
meczu W\_e = Oczekiwany rezultat meczu

    elo_ranks <- calculate_elo_ranks(team_names, league_data)

    elo <- calculate_elo_result(elo_ranks, validation_data)
    elo

    ##   Dokladnosc   Zysk
    ## 1   50.39474 -380.6

ELO + Statystyczna
==================

    combined <- init_result_data_frame()

    for(i in 1:length(validation_data)) {
      for(j in 1:nrow(validation_data[[i]])) {
          ind_home <- match(validation_data[[i]][j, "HomeTeam"], teams$Team)
          ind_away <- match(validation_data[[i]][j, "AwayTeam"], teams$Team)
          
          ind_elo_home <- match(validation_data[[i]][j, "HomeTeam"], elo_ranks$Team)
          ind_elo_away <- match(validation_data[[i]][j, "AwayTeam"], elo_ranks$Team)
          
          # Jesli obydwie druzyny zostaly znalezione - mozemy testowac hipoteze 
          if(!is.null(ind_home) & !is.null(ind_away)) {
            # Zakres dla Home/Away
            win_ha_prob <- home_win_weight * teams$HomePercentageWin[ind_home] + away_loose_weight * teams$AwayPercentageLoose[ind_away]
            draw_ha_prob <- home_draw_weight * teams$HomePercentageDraw[ind_home] + away_draw_weight * teams$AwayPercentageDraw[ind_away]
            
            elo_win_a <- 0.448 + 0.0053 * (elo_ranks[ind_elo_home, "Rank"] - elo_ranks[ind_elo_away, "Rank"])
            predicted_score <- get_score((win_ha_prob + elo_win_a)/2, draw_ha_prob)
            is_guessed <- validation_data[[i]][j, "FTR"] == predicted_score
            profit <- get_profit(validation_data[[i]][j, ], predicted_score, is_guessed)
            
            combined <- rbind(combined, data.frame(HomeTeam = validation_data[[i]][j, "HomeTeam"], 
                                                                   AwayTeam = validation_data[[i]][j, "AwayTeam"],
                                                                   PrognosedResult = predicted_score, 
                                                                   RealResult = validation_data[[i]][j, "FTR"], 
                                                                   Guessed = is_guessed,
                                                                   Profit = profit))
          }

      }
    }

    elo_stats <- calculate_guessed_statistics(combined)
    elo_stats

    ##   Dokladnosc  Zysk
    ## 1   54.73684 115.3

Model z wykorzystaniem rozkładu Poissona
========================================

Rozkład Poissona może zostać wykorzystany zarówno do wyznaczenia
najbardziej prawdopodobnego wyniku jak i rezultatu H/D/A

    poisson_stats <- calculate_poisson_statistics(team_names, league_data)


    average_scores_home <- get_average_scores(league_data, "FTHG")
    average_scores_away <- get_average_scores(league_data, "FTAG")
      
    p_res <- calculate_poisson_results(poisson_stats, validation_data, average_scores_home, average_scores_away)
    p_res

    ##   Dokladnosc   Zysk
    ## 1   55.52632 -233.5

Wyniki
======

Zestawiając wyniki różnych metod mamy:

    # Statystyczna Win-Draw-Loose
    h_d_a

    ##   Dokladnosc Zysk
    ## 1   49.21053  112

    # Statystyczna z podziałem na rezultaty Home/Away
    a <- data.frame(mean(h_a_fixed$Dokladnosc), mean(h_a_fixed$Zysk))
    colnames(a) <- colnames(h_a_fixed)
    a

    ##   Dokladnosc Zysk
    ## 1   55.78947 87.4

    # Statystyczna z podziałem na rezultaty Home/Away + randomizacja
    a <- data.frame(mean(h_a_randomized$Dokladnosc), mean(h_a_randomized$Zysk))
    colnames(a) <- colnames(h_a_randomized)
    a

    ##   Dokladnosc  Zysk
    ## 1   54.47368 -60.4

    # ELO
    elo

    ##   Dokladnosc   Zysk
    ## 1   50.39474 -380.6

    # Mieszana ELO + statystyczna 
    elo_stats

    ##   Dokladnosc  Zysk
    ## 1   54.73684 115.3

    # Poisson
    p_res

    ##   Dokladnosc   Zysk
    ## 1   55.52632 -233.5

Jak widać, w najczęstszym przypadku to podział posiadanych danych na
rezultaty U siebie/na wyjeździe dawały najbardziej obiecujące rezultaty
zarówno do skuteczności jak i do zysków.

Wnioski
=======

Przewidywanie wyników piłki nożnej jest niezwykle trudnym zadaniem. Nam
udało sie to w 50-55% dla różnych zbiorów danych, w zależności od
wybranego modelu. Wynik ten bez odliczenia podatku od wygranych pozwala
już odnieść pewien sukces finansowy. Z uwagi na dużą losowość czynników
mogących mieć wpływ na ostateczny rezultat nie sposób ocenić jest z
dużym prawdopodobieństwem która drużyna może odnieść sukces.
