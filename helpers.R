zerofy <- function(x) {
  if(x < 10) 
    return(paste("0", x, sep = ""))
  return(x)
}

read_scores <- function(league, scores_season_range) {
  scores_data <- c()
  for(i in data_range) {
    index <- i - data_range[1] + 1
    path <- paste("Data/", league, "/20", zerofy(i), "-", zerofy(i+1), ".csv", sep = "");
    scores_data[[index]] <- read.csv(path)
    scores_data[[index]] <- scores_data[[index]][scores_data[[index]]$HomeTeam != "", ]
  }
  return(scores_data)
}

find_unique_teams <- function(match_data) {
  unique_teams <- c()
  for(i in 1:length(match_data)) {
    unique_teams <- unique(rbind(unique_teams, match_data[[i]]["HomeTeam"]))
  }

  filtered_teams <- data.frame(Team = unique_teams$HomeTeam[unique_teams$HomeTeam != ""])
  return(filtered_teams)
}

calculate_total <- function(data, suffix) {
  return (data[paste(suffix, "Win", sep ="")]  + data[paste(suffix, "Draw", sep ="")] + data[paste(suffix, "Loose", sep ="")])
}

init_result_data_frame <- function() {
  return(data.frame(HomeTeam = character(), AwayTeam = character(), PrognosedResult = character(), RealResult = character(), Guessed = logical(), 
                    Profit = double()))
}

get_score <- function(w_prob, d_prob) {
  if(is.na(w_prob) || is.na(d_prob)) {
    return("U")
  }
  l_prob <- 1 - w_prob - d_prob
  if( l_prob > w_prob & l_prob > d_prob)
    return("A")
  else if(w_prob > l_prob & w_prob > d_prob)
    return("H")
  return("D")
}

get_profit <- function(bets, prediction, guessed) {
  factor <- 10
  if(guessed == TRUE) {
    # Podatek od wygranej
    factor <- factor * 1
    if(prediction == "H") {
      return(factor*(bets$B365H - 1))
    }
    else if(prediction == "D") {
      return(factor * (bets$B365D - 1))
    }
    else {
      return(factor * (bets$B365A - 1))
    }
  }
  return(-factor)
}

calculate_guessed_statistics <- function(results) {
  guessed <- 0
  profit <- 0
  for(i in 1:nrow(results)) {
    if(results$Guessed[i] == TRUE) {
      guessed <- guessed + 1
    }
    profit <- profit + results$Profit[i]
  }
  stats <- data.frame(guessed/nrow(results) * 100, profit)
  colnames(stats) <- c("Dokladnosc", "Zysk")
  return(stats)
}