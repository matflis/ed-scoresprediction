Predykcja wynikow meczy pilki noznej
========================================================
author: M & K
date: 24-01-2017
autosize: true

Skad pomysl?
========================================================

- Duze zainteresowanie
- Milionerzy przed 30-tka
- Analiza danych :-)

Zrodlo danych
========================================================

http://www.football-data.co.uk/data.php

Liga hiszpanska

Dane treningowe - sezon 14-16

Dane walidujace - sezon 16/17

```
   HomeTeam   AwayTeam FTHG FTAG FTR
1   Almeria    Espanol    1    1   D
2   Granada  La Coruna    2    1   H
3    Malaga Ath Bilbao    1    0   H
4   Sevilla   Valencia    1    1   D
5 Barcelona      Elche    3    0   H
6     Celta     Getafe    3    1   H
```

Statystyki
========================================================

![plot of chunk unnamed-chunk-2](final_presentation-figure/unnamed-chunk-2-1.png)


Model na bazie Wygrana-Remis-Przegrana
====================================================


Zliczanie statystyk i wnioskowanie na podstawie funkcji prawdopodobienstwa

Wygrana = Odsetek zwyciestw

Remis = Odsetek remisow

Przegrana = Reszta



```
  Dokladnosc Zysk
1   49.21053  112
```


Podzial na rezultaty "U siebie"/"Na wyjezdzie" - bez randomizacji
======================================================


Wygrana - 3/5 * HomeWin + 1/2 * AwayLoose

Remis - 1/2 * HomeDraw + 1/2 * AwayDraw

Porazka - Reszta


```
  Dokladnosc Zysk
1   56.57895 77.0
2   55.78947 59.4
3   56.57895 77.0
4   55.78947 59.4
```

Podzial na rezultaty "U siebie"/"Na wyjezdzie" - randomizacja
======================================================



```
  Dokladnosc  Zysk
1   57.10526  90.1
2   56.31579 166.6
3   56.31579   8.4
4   55.26316  46.2
```



Wykorzystanie rang - ELO
======================================================

Wzor na nowy ranking: 

- R_n = R_o + K*G * (W - W_e) 


```
  Dokladnosc   Zysk
1   50.39474 -380.6
```



Wykorzystanie rozkladu Poissona
======================================================


```
  Dokladnosc   Zysk
1   55.52632 -233.5
```


Zestawienie
======================================================


```
  Dokladnosc Zysk
1   49.21053  112
```

```
  Dokladnosc Zysk
1   56.18421 68.2
```

```
  Dokladnosc   Zysk
1      56.25 77.825
```

```
  Dokladnosc   Zysk
1   50.39474 -380.6
```

```
  Dokladnosc   Zysk
1   55.52632 -233.5
```


Przyszly weekend
==========================================================

![alt text](Untitled.png)




