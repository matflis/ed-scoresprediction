---
title: "Przewidywanie wynik�w mecz�w pi�karkich"
author: "Mateusz Flis, Karolina Kosek"
date: "29 pa�dziernika 2016"
output: md_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
require("ggplot2")
require("RColorBrewer")
set.seed(NULL)

source("helpers.R")
source("naive_method_helpers.R")
source("elo_helpers.R")
source("poisson_helpers.R")

```
## 1. Cel projektu
Celem naszego projektu by�o zbadanie na do jakiego stopnia jeste�my wstanie przewidzie� rezultaty mecz�w zespo��w pi�karskich wybranej ligi, oraz ile na tym mo�emy potencjalnie na tym zarobi�.

## 2. Generowanie danych, dane surowe i dane poprawne technicznie
Wszelkie dane pochodz� z: http://www.football-data.co.uk/data.php.
Ze wzgl�du na to, �e ilo�� plik�w z danymi nie by�a znacz�ca, dane w plikach .csv zosta�y pobrane r�cznie. Dodatkowo napisali�my skrypt s�u��cy do pobrania przysz�ych danych i przetworzenia danych do formy czytelnej dla programu R Studio.

W naszym przypadku dane surowe by�y r�wnowa�ne z danymi poprawnymi technicznie.

Formula na pobieranie danych www.football-data.co.uk/mmz4281/SEZON/LIGA.csv
- Premier League - "E0""
- SerieA - "D1"
- Primiera Division - "SP1"
- Bundesliga - "D1"

Przykladowy url dla sezonu 09/10 dla Premier League - www.football-data.co.uk/mmz4281/0910/E0.csv

# 3. Czyszczenie danych
```{r data_load}

league <- c('Bundesliga', 'Bundesliga 2', 'LaLiga', 'PremierLeague', 'Championship', 'SerieA', 'Ligue 1', 'Turcja')
# Wybrana liga do analizy
choice <- 3
# Zakres danych doczytaczych wynikow
data_range <- 14:15
# Zakres danych walidacyjnych
validation_data_range <- 16:16

league_data <- read_scores(league[choice], data_range)
validation_data <- read_scores(league[choice], validation_data_range)

total_data <- c(league_data, validation_data)

```

## 4. Analiza danych

Statystyki procentowe poszczeg�lnych zespo��w prezentuj� sie nast�puj�co.

``` {r statistics}

team_names <- find_unique_teams(league_data)

teams <- calculate_scores_statistics(team_names, league_data)

## Percentage comparison between Win-Draw-Loose (Home)


barplot(t(teams[,5:7]),
        names.arg = teams$Team,
        cex.names=1,
        las=2,
        col=c("blue", "green", "red"),
        #xlab="Team",
        ylab="Win | Draw | Loose scores",
        xlim=c(0,30),width=1,
        ylim=c(0,1))

title("Win-Draw-Loose (Home)")
legend("topright",
       legend = colnames(teams[5:7]), pch=15, col=c("blue", "green", "red"))

## Percentage comparison between Win-Draw-Loose (Away)

barplot(t(teams[,11:13]),
        names.arg = teams$Team,
        cex.names=1,
        las=2,
        col=c("blue", "green", "red"),
        #xlab="Team",
        ylab="Win | Draw | Loose scores",
        xlim=c(0,30),width=1,
        ylim=c(0,1))

title("Win-Draw-Loose (Away)")
legend("topright",
       legend = colnames(teams[11:13]), pch=15, col=c("blue", "green", "red"))

## Home vs. Away Scores - Win
barplot(t(teams[,c(2,8)]),
        names.arg = teams$Team,
        cex.names=1,
        las=2,
        col=c("blue","red"),
        #xlab="Team",
        ylab="Home/Away scores",
        xlim=c(0,70),width=1,
        ylim=c(0,max(teams$HomeWin,teams$AwayWin)),
        beside= TRUE)

title("Win - Home vs. Away")
legend("topright", 
       legend = colnames(teams[c(2,8)]), pch=15, col=c("blue","red"))

## Home vs. Away Scores - Draw
barplot(t(teams[,c(3,9)]),
        names.arg = teams$Team,
        cex.names=1,
        las=2,
        col=c("blue","red"),
        #xlab="Team",
        ylab="Home/Away scores",
        xlim=c(0,70),width=1,
        ylim=c(0,max(teams$HomeDraw,teams$AwayDraw)),
        beside= TRUE)

title("Draw - Home vs. Away")
legend("topright", 
       legend = colnames(teams[c(3,9)]), pch=15, col=c("blue","red"))


## Home vs. Away Scores - Loose
barplot(t(teams[,c(4,10)]),
        names.arg = teams$Team,
        cex.names=1,
        las=2,
        col=c("blue","red"),
        #xlab="Team",
        ylab="Home/Away scores",
        xlim=c(0,70),width=1,
        ylim=c(0,max(teams$HomeLoose,teams$AwayLoose)),
        beside= TRUE)

title("Loose - Home vs. Away")
legend("topright", 
       legend = colnames(teams[c(4,10)]), pch=15, col=c("blue","red"))


## Og�lne por�wnanie wynik�w dru�yn - Home
 
 barplot(t(teams[,2:4]),
         names.arg = teams$Team,
         cex.names=1,
         las=2,
         col = c("blue", "green", "red"),
         #xlab="Team",
         ylab="Win | Draw | Loose scores",
         xlim=c(0,90),width=1,
         ylim=c(0,max(teams$HomeWin,teams$HomeDraw,teams$HomeLoose)),
         beside= TRUE)

 title("Win-Draw-Loose (Home)")
legend("topright", 
         legend = colnames(teams[2:4]), pch=15, col = c("blue", "green", "red"))


 ## Og�lne por�wnanie wynik�w dru�yn - Away

 barplot(t(teams[,8:10]),
         names.arg = teams$Team,
         cex.names=1, 
         las=2,
         col = c("blue", "green", "red"),
         #xlab="Team",
         ylab="Win | Draw | Loose scores",
         xlim=c(0,90),width=1,
         ylim=c(0,max(teams$AwayWin,teams$AwayDraw,teams$AwayLoose)),
         beside= TRUE)

 title("Win-Draw-Loose (Away)")
legend("topright", 
         legend = colnames(teams[8:10]), pch=15, col = c("blue", "green", "red"))

```

# 5. Testy
# Statystyka Win-Draw-Loose

Zliczanie statystyk i wnioskowanie na podstawie funkcji prawdopodobie�stwa
Wzory to:
Sukces - 3/5 * HomeWin + 1/2 * AwayLoose
Remis - 1/2 * HomeDraw + 1/2 * AwayDraw
Pora�ka - Reszta

Dodatkowo zosta�a uwzgl�dniona randomizacja - tj. dla niewielkiej r�nicy statystyk % mo�liwy jest inny wynik ni� ten zgodny z zasad� najwy�szego prawdopodobie�stwa.

```{r naive_method}
home_win_weight <- 3/5
home_draw_weight <-  1/2

away_loose_weight <- 1/2
away_draw_weight <- 1/2

h_d_a <- calculate_naive_result(teams, validation_data)
h_d_a

h_a_fixed <- data.frame()
h_a_randomized <- data.frame()

for(i in 1:length(total_data)) {
  if(i < length(total_data)) {
    training_data <- total_data[(i+1):length(total_data)]
  }
  else {
    training_data <- c()
  }
  if(i > 1) {
    training_data <- c(total_data[1:i - 1], training_data)
  }
  val_data <- total_data[i]
  team_stats <- calculate_scores_statistics(team_names, training_data)
  h_a_fixed <- rbind(h_a_fixed, calculate_naive_ha_result(team_stats, val_data, home_win_weight, home_draw_weight, away_draw_weight, away_loose_weight, FALSE))
  h_a_randomized <- rbind(h_a_randomized, calculate_naive_ha_result(team_stats, val_data, home_win_weight, home_draw_weight, away_draw_weight, away_loose_weight, TRUE))
}

h_a_fixed
h_a_randomized

```

# Metoda z wykorzystaniem rang - ELO

Dok�adny algorytm opisany jest na - http://footballdatabase.com/methodology.php

Generalny wz�r na nowy ranking to: 
- R_n = R_o + K*G * (W - W_e) 
  Gdzie;
    R_n = Nowy rating
    R_o = Stary rating
    K = Waga meczu (w zaleznosci od poziomu rozgrywek)
    G = Index r�nicy goli
    W = Rezultat meczu
    W_e = Oczekiwany rezultat meczu

```{r elo}

elo_ranks <- calculate_elo_ranks(team_names, league_data)

elo <- calculate_elo_result(elo_ranks, validation_data)
elo

```
# ELO + Statystyczna

```{r combined_elo_statistic}

combined <- init_result_data_frame()

for(i in 1:length(validation_data)) {
  for(j in 1:nrow(validation_data[[i]])) {
      ind_home <- match(validation_data[[i]][j, "HomeTeam"], teams$Team)
      ind_away <- match(validation_data[[i]][j, "AwayTeam"], teams$Team)
      
      ind_elo_home <- match(validation_data[[i]][j, "HomeTeam"], elo_ranks$Team)
      ind_elo_away <- match(validation_data[[i]][j, "AwayTeam"], elo_ranks$Team)
      
      # Jesli obydwie druzyny zostaly znalezione - mozemy testowac hipoteze 
      if(!is.null(ind_home) & !is.null(ind_away)) {
        # Zakres dla Home/Away
        win_ha_prob <- home_win_weight * teams$HomePercentageWin[ind_home] + away_loose_weight * teams$AwayPercentageLoose[ind_away]
        draw_ha_prob <- home_draw_weight * teams$HomePercentageDraw[ind_home] + away_draw_weight * teams$AwayPercentageDraw[ind_away]
        
        elo_win_a <- 0.448 + 0.0053 * (elo_ranks[ind_elo_home, "Rank"] - elo_ranks[ind_elo_away, "Rank"])
        predicted_score <- get_score((win_ha_prob + elo_win_a)/2, draw_ha_prob)
        is_guessed <- validation_data[[i]][j, "FTR"] == predicted_score
        profit <- get_profit(validation_data[[i]][j, ], predicted_score, is_guessed)
        
        combined <- rbind(combined, data.frame(HomeTeam = validation_data[[i]][j, "HomeTeam"], 
                                                               AwayTeam = validation_data[[i]][j, "AwayTeam"],
                                                               PrognosedResult = predicted_score, 
                                                               RealResult = validation_data[[i]][j, "FTR"], 
                                                               Guessed = is_guessed,
                                                               Profit = profit))
      }

  }
}

elo_stats <- calculate_guessed_statistics(combined)
elo_stats
```


# Model z wykorzystaniem rozk�adu Poissona

Rozk�ad Poissona mo�e zosta� wykorzystany zar�wno do wyznaczenia najbardziej prawdopodobnego wyniku jak i rezultatu H/D/A

```{r Poisson}
poisson_stats <- calculate_poisson_statistics(team_names, league_data)


average_scores_home <- get_average_scores(league_data, "FTHG")
average_scores_away <- get_average_scores(league_data, "FTAG")
  
p_res <- calculate_poisson_results(poisson_stats, validation_data, average_scores_home, average_scores_away)
p_res
```


# Wyniki 

Zestawiaj�c wyniki r�nych metod mamy:

```{r Results}

# Statystyczna Win-Draw-Loose
h_d_a

# Statystyczna z podzia�em na rezultaty Home/Away
a <- data.frame(mean(h_a_fixed$Dokladnosc), mean(h_a_fixed$Zysk))
colnames(a) <- colnames(h_a_fixed)
a

# Statystyczna z podzia�em na rezultaty Home/Away + randomizacja
a <- data.frame(mean(h_a_randomized$Dokladnosc), mean(h_a_randomized$Zysk))
colnames(a) <- colnames(h_a_randomized)
a
# ELO
elo

# Mieszana ELO + statystyczna 
elo_stats

# Poisson
p_res

```
Jak wida�, w najcz�stszym przypadku to podzia� posiadanych danych na rezultaty U siebie/na wyje�dzie dawa�y najbardziej obiecuj�ce rezultaty zar�wno do skuteczno�ci jak i do zysk�w.

# Wnioski

Przewidywanie wynik�w pi�ki no�nej jest niezwykle trudnym zadaniem. Nam uda�o sie to w 50-55% dla r�nych zbior�w danych, w zale�no�ci od wybranego modelu. Wynik ten bez odliczenia podatku od wygranych pozwala ju� odnie�� pewien sukces finansowy.
Z uwagi na du�� losowo�� czynnik�w mog�cych mie� wp�yw na ostateczny rezultat nie spos�b oceni� jest z du�ym prawdopodobie�stwem kt�ra dru�yna mo�e odnie�� sukces.
